﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace H.W_12._8._20
{
    public class Person : IComparable
    {
        public int Id { get; private set; }
        public int Age { get; private set; }
        public float Height { get; private set; }
        public string Name { get; private set; }
        private readonly static IComparer idComparer;
        private readonly static IComparer ageComparer;
        private readonly static IComparer heightComparer;
        private readonly static IComparer nameComparer;
        public static IComparer IdComparer { get; }
        public static IComparer AgeComparer { get; }
        public static IComparer NameComparer { get; }
        public static IComparer HeightComparer { get; }
        static Person()
        {
            idComparer = Person.IdComparer;
            ageComparer = Person.AgeComparer;
            heightComparer = Person.HeightComparer;
            nameComparer = Person.NameComparer;
        }
        public Person(int id, int age, float height, string name)
        {
            Id = id;
            Age = age;
            Height = height;
            Name = name;
        }
        public int CompareTo(object obj)
        {
            return this.Id - ((Person)obj).Id;
        }
        public override string ToString()
        {
            return $"Id: {Id} Name: {Name} Height: {Height} Age: {Age}";
        }
    }
}
