﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_12._8._20
{
    class Program
    {
        static void PrintPersonArray(Person[] personArray)
        {
            foreach (Person person in personArray)
            {
                Console.WriteLine(person.ToString());
            }
        }
        static void Main(string[] args)
        {
            Person[] people = new Person[5]
            {
                new Person(3,12, 1.80f, "david"),
                new Person(2,16, 1.90f, "sami"),
                new Person(5,20, 1.55f, "nirit"),
                new Person(1,18, 1.70f, "shahar"),
                new Person(4,19, 1.75f, "ben")
            };
            PrintPersonArray(people);
            Array.Sort(people);
            PrintPersonArray(people);
            Array.Sort(people, new PersonCompareByName());
            PrintPersonArray(people);
            Array.Sort(people, Person.NameComparer);
            PrintPersonArray(people);
            Array.Sort(people, Person.HeightComparer);
            PrintPersonArray(people);
            Array.Sort(people, Person.IdComparer);
            PrintPersonArray(people);
            Array.Sort(people, Person.AgeComparer);
            PrintPersonArray(people);
        }
    }
}
