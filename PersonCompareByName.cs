﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_12._8._20
{
    public class PersonCompareByName : IComparer
    {
        public int Compare(object x, object y)
        {
            Person p1 = (Person)x;
            Person p2 = (Person)y;
            return p1.Name.CompareTo(p2.Name);
        }
    }
}
